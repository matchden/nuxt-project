
## Introduction
A simple and clean template made with [Nuxt.js](https://nuxtjs.org/)

#### [DEMO](https://musing-bassi-0c1afe.netlify.com/)

#### Requirements
- [Node.js](https://nodejs.org/en/) - To preview the application, we require a development server, which will be using node,js as the language. We also require it to run npm(node package manager) to manage packages that are local dependencies of a particular project, as well as globally-installed **JavaScript** tools. 
- [Bulma](https://bulma.io/) - A free, open source CSS framework based on  **Flexbox**  and used by more than  **100,000**developers.

#### Important links
- [create-nuxt-app](https://github.com/nuxt/create-nuxt-app)
- [Nuxt.js](https://nuxtjs.org/)
- [Vue.js](https://vuejs.org/)
- [Sass](https://sass-lang.com/)
- [node-sass](https://www.npmjs.com/package/node-sass) 
- [sass-loader](https://github.com/webpack-contrib/sass-loader)


# Setup

Open the terminal in the folder you want to create your project.

#### Install create-nuxt-app globally
```bash
npm install -g create-nuxt-app
```
#### Create the project
```bash
create-nuxt-app project-name
```
![](https://ik.imagekit.io/iwilfried/nuxt-alpha-01_ByeRS0AOMV.png)
1. *Project name* - Give your project a name.
2. *Project description* - Describe what your project is about.
3.  *Use a custom server framework* - Choose between integrated server-side frameworks to support the development of web applications including web services, web resources and web APIs i.e. -   None (Nuxt default server), [Express](https://expressjs.com/), [Koa](https://koajs.com/), [AdonisJS](https://adonisjs.com/), [HapiJS](https://hapijs.com/), [Feathers](https://feathersjs.com/), [MicroJS](http://microjs.com/).
4. *Use a custom UI Framework* - Choose your favorite UI framework  to use their inbuilt UI Components i.e. -   None (feel free to add one later), [Bootstrap](https://getbootstrap.com/), [Vuetify](https://vuetifyjs.com/en/), [Bulma](https://bulma.io/), [Tailwind](https://tailwindcss.com/), [Element UI](https://element.eleme.io/#/en-US).
5. *Choose rendering mode* - *Universal* to describe JavaScript code that can execute both on the client and the server side.  *SPA* to describe JavaScript code that can execute on the client side. 
6. *Use axios module* - Add  [axios module](https://github.com/nuxt-community/axios-module)  to make HTTP request easily into your application..
7. *Use eslint* - Add  [EsLint](https://eslint.org/)  to Lint your code on save.
8. *Use prettier* - Add  [Prettier](https://prettier.io/)  to prettify your code on save.
9. *Author name* - Enter your name.
10. *Choose package manager* - Choose between your package manager to handle dependencies {yarn}(https://yarnpkg.com/en/) or [npm](https://www.npmjs.com/).

![](https://ik.imagekit.io/iwilfried/nuxt-alpha-02_ryTYZytzV.png)

```bash
# change directory 
$ cd project-name

# launch app
$ npm run dev
```
The application is now running on http://localhost:3000.

> Nuxt.js will listen for file changes inside the pages directory, so there is no need to restart the application when adding new pages.

To discover more about the directory structure of the project: [Directory Structure Documentation](https://nuxtjs.org/guide/directory-structure).

## Setting up the requirements
### [Bulma](https://bulma.io/)
You can either choose the CSS or Sass version of it. We are going to use [Sass](https://sass-lang.com/) for this project as it consists of **39**  `.sass` files that we can import **individually.**

To customize Bulma, you will need to:

-   **install**  (or download) Bulma
-   have a working  **Sass setup**
-   create your own  `.scss`  or  `.sass`  file

To achieve this, open your terminal:
```bash
# Install node-sass and sass-loader
$ npm install --save-dev node-sass sass-loader
```
Now you will be able to see that there are node-sass and sass-loader are added in the package.json.
- Go to the assets directory and create a folder named **vendors** and paste in the bulma folder we downloaded.
- Create another folder in the assets directory and name it **sass** and create a file named styles.scss ( here all our styling goes ) in it.
- Import Bulma in your styles.scss. You can also import only the components that are required in the project.
```sass
@import  "~assets/vendors/bulma/bulma.sass";
```
-   Update  `nuxt.config.js`:
```javascript
/*  
  ** Global CSS  
  */  
  css: [  
    { src:  '~/assets/sass/styles.scss', lang:  'scss' }
  ],
```
Now you can use bulma in your projects.

> Note: the leading `~` in all file paths above is webpack’s way of saying “the root project folder” so `~/assets` is just the folder called `assets` created by nuxt in the root level of the project.
