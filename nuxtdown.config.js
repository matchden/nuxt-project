module.exports = {
  api: function(isStatic) {
    const baseURL = "http://localhost:3000";
    const browserBaseURL = !isStatic ? "" : process.env.BROWSER_BASE_URL;

    return {
      baseURL,
      browserBaseURL
    };
  },
  content: {
    page: "/posts/_slug",
    permalink: "/posts/:slug",
    isPost: false
  }
};
